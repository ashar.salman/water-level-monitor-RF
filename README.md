# Water Level Monitor

This is proof of concept project for water level monitoring in roof top water tank.
## Getting Started

This is useful where there is no wifi and you need around 100 ft range.
The project consist of two parts, tranmitter and receiver.

### Prerequisites

Transmitter:

```
1. Arduino Mega
2. HC12
3. JSN-SR04T Waterproof Ultrasonic Module
```

Receiver:

```
1. Arduino Mega
2. HC12
3. DS3231 Real time clock
4. 3.2 inch TFT LCD screen module Ultra HD 320X480 for MEGA 2560 R3 Board
```
### Installing

Connect/solder the compnents and upload the code.


## Deployment

Place the ultrasound sensor just above the water tank opening. The transmitter will start tranmitting the water level and you will be able to see it graphically on receiver.

## Results

I tested it over three stories above tank and it worked nice.



