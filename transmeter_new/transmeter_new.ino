/*  HC12 Send/Receive Example Program 1
    By Mark J. Hughes
    for AllAboutCircuits.com

    This code will automatically detect commands as sentences that begin
    with AT and both write them and broadcast them to remote receivers
    to be written.  Changing settings on a local transceiver will
    change settings on a remote receiver.

    Connect HC12 "RXD" pin to Arduino Digital Pin 4
    Connect HC12 "TXD" pin to Arduino Digital Pin 5
    Connect HC12 "Set" pin to Arduino Digital Pin 6

    Do not power HC12 via Arduino over USB.  Per the data sheet,
    power the HC12 with a supply of at least 100 mA with
    a 22 uF to 1000 uF reservoir capacitor and connect a 1N4007 diode in
    series with the positive supply line if the potential difference exceeds 4.5 V.

    Upload code to two Arduinos connected to two computers
    that are separated by at least several meters.

*/


#include <NewPing.h>
#include <SoftwareSerial.h>
#include <SPI.h> // Not actually used but needed to compile

const byte HC12RxdPin = 4;                      // "RXD" Pin on HC12
const byte HC12TxdPin = 5;                      // "TXD" Pin on HC12
const byte HC12SetPin = 6;                      // "SET" Pin on HC12

unsigned long timer = millis();                 // Delay Timer

char SerialByteIn;                              // Temporary variable
char HC12ByteIn;                                // Temporary variable
String HC12ReadBuffer = "";                     // Read/Write Buffer 1 for HC12
String SerialReadBuffer = "";                   // Read/Write Buffer 2 for Serial
boolean SerialEnd = false;                      // Flag to indicate End of Serial String
boolean HC12End = false;                        // Flag to indiacte End of HC12 String
boolean commandMode = false;                    // Send AT commands

// Software Serial ports Rx and Tx are opposite the HC12 Rx and Tx
// Create Software Serial Port for HC12
SoftwareSerial HC12(10, 11); // HC-12 TX Pin, HC-12 RX Pin


#define TRIGGER_PIN 7
#define ECHO_PIN 6
#define MAX_DISTANCE 200


NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

struct dataStruct {
  int distance;
  float temp;
  unsigned long counter;

} myData;

byte tx_buf[sizeof(myData)] = {0};

void setup() {
  
  Serial.begin(9600);                           // Open serial port to computer
  HC12.begin(9600);

  myData.distance = 0;
  myData.temp = 22.394;
}

void loop() {
  delay(500);
  unsigned int uS = sonar.ping_cm();
  Serial.print(uS);
  Serial.println("cm");

  if (uS > 0) {
    myData.distance = uS;

    String strDist = String(uS);
    HC12.print(uS);
    myData.counter++;

    delay(2000);
  }

}
